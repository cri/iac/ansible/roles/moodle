Moodle
=========

This role deploys a Moodle instance.

Requirements
------------

This role requires a working PostgreSQL database server to back the Moodle
database.

Role Variables
--------------

A description of the settable variables for this role should go here, including
any variables that are in defaults/main.yml, vars/main.yml, and any variables
that can/should be set via parameters to the role. Any variables that are read
from other roles and/or the global scope (ie. hostvars, group vars, etc.) should
be mentioned here as well.

Dependencies
------------

This role depends on cri.docker, a role that deploys Docker and
geerlingguy.redis to deploy a redis instance.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: forge.moodle, x: 42 }

License
-------

MIT

Author Information
------------------

This role was originally created by [Kévin Sztern](https://kevinsztern.fr/).
